# Принято: 4

n, m = input().split()
if int(n) > 1:
    print(int(m) * "A")
    for i in range(int(n) - 2):
        print("A" + " " * (int(m)- 2) + "A")
    print(int(m) * "A")
else:
    print(int(m) * "A")
# Перевод строки в число - довольно затратная операция
# Лучше сделать это сразу
print(int(m) * "A")
for i in range(int(n) - 2):
    print("A" + " " * (int(m)- 2) + "A")
print(int(m) * "A")
