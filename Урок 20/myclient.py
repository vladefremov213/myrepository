import socket

from threading import Thread


def request():
    while sock._closed is False:
        request = input()
        sock.send(request.encode("utf-8"))
        print(f"client > '{request}'")
        if request.lower().strip() == "bye":
            sock.close()
            print(f"Close connection with {addr[0]}:{addr[1]}")
            break


def response():
    while sock._closed is False:
        response = sock.recv(1024).decode("utf-8")
        print(f"server > '{response}'")
        if response.lower().strip() == "bye":
            sock.close()
            print(f"Close connection with {addr[0]}:{addr[1]}")
            break


sock = socket.socket()
addr = ("127.0.0.1", 8000)
sock.connect(addr)
print(f"{addr[0]}:{addr[1]} connected")
thread1 = Thread(target=request)
thread2 = Thread(target=response)
thread1.start()
thread2.start()
