import socket

from threading import Thread


def request():
    while sock._closed is False:
        request = conn.recv(1024).decode("utf-8")
        print(f"client > '{request}'")
        if request.lower().strip() == "bye":
            sock.close()
            print(f"Close connection with {addr[0]}:{addr[1]}")
            break

        
def response():
    while sock._closed is False:
        response = input()
        conn.send(response.encode("utf-8"))
        print(f"server > '{response}'")
        if response.lower().strip() == "bye":
            sock.close()
            print(f"Close connection with {addr[0]}:{addr[1]}")
            break


sock = socket.socket()
sock.bind(("", 8000))
sock.listen(1)
conn, addr = sock.accept()
print(f"{addr[0]}:{addr[1]} connected")
thread1 = Thread(target=request)
thread2 = Thread(target=response)
thread1.start()
thread2.start()
