# Принято

import sqlalchemy as sa
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy.sql import func

Base = declarative_base()


class CommonMixture:
    id = sa.Column(sa.Integer, primary_key=True, autoincrement=True)


class Material(CommonMixture, Base):
    __tablename__ = "Materials"
    name = sa.Column(sa.Unicode(64), unique=True, nullable=False)


class Price(CommonMixture, Base):
    __tablename__ = "Prices"

    price = sa.Column(sa.Float, sa.CheckConstraint("price>=0"), nullable=False)
    material_id = sa.Column(
        sa.Integer,
        sa.ForeignKey(f"{Material.__tablename__}.id"),
        # Ставьте запятую даже после последнего аргумента - это удобно
        nullable=False
    )


class Sale(CommonMixture, Base):
    __tablename__ = "Sales"

    material_id = sa.Column(
        sa.Integer,
        sa.ForeignKey(f"{Material.__tablename__}.id"),
        nullable=False
    )
    amount = sa.Column(sa.Float, nullable=False)


def add(function):
    def wrapper(self, *args, **kwargs):
        record = function(self, *args, **kwargs)
        self._session.add(record)
        self._session.commit()
        return record
    return wrapper


class Role:
    def __init__(self, engine, name, host=None, port=None, username=None, password=None, **params):
        dsn = f"{engine}://"
        if username is not None:
            dsn += username
        if password is not None:
            dsn += f":{password}"
        if username is not None:
            dsn += "@"
        if host is not None:
            dsn += f"{host}"
        if port is not None:
            dsn += f":{port}"
        dsn += f"/{name}"
        if params:
            dsn += "?"
        dsn += "&".join(f"{key}={value}" for key, value in params.items())

        self._engine = sa.create_engine(dsn)
        Base.metadata.create_all(self._engine)
        self._session = sessionmaker(bind=self._engine)()


class SellerRole(Role):
    def get_material(self, name):
        return self._session.query(Material).filter(Material.name == name).first()

    def get_amount_material(self, material):
        return self._session.query(func.sum(Sale.amount).label("amount")).filter(
            Sale.material_id == material.id,
        ).first()[0]

    @add
    def sale(self, name, amount):
        material = self.get_material(name)
        if material is None:
            raise Exception("Has no product with name '%s'" % name)
        has_amount = self.get_amount_material(material)
        if amount > has_amount:
            raise Exception("Has no %f amount for sell '%s'. Current amount: %f" % (
                amount, name, has_amount,
            ))
        return Sale(material_id=material.id, amount=-amount)


class BuyerRole(Role):
    def get_material(self, name):
        return self._session.query(Material).filter(Material.name == name).first()

    @add
    def income(self, name, amount,):
        material = self.get_material(name)
        if material is None:
            raise Exception("Only boss can add new material")
        return Sale(material_id=material.id, amount=amount)
# Между классами 2 пустые строки

class BossRole(SellerRole, BuyerRole):
    # Вот income метод бы и переопределили бы
    # Можно было сделать по методу на добавление материала и на установку новой цены для материала
    # Мне бы ещё было бы интересно посмотреть на получение актуальной цены материала
    @add
    def bossincome(self, name, amount, price):
        material = Material(name=name)
        self._session.add(material)
        self._session.commit()
        price = Price(price=price, material_id=material.id)
        self._session.add(price)
        self._session.commit()

        return Sale(material_id=material.id, amount=amount)


boss = BossRole(engine="sqlite", name="store.db")
seller = SellerRole(engine="sqlite", name="store.db")
buyer = BuyerRole(engine="sqlite", name="store.db")
boss.bossincome(name="Бетон", amount=100, price=1000)
boss.bossincome(name="Обои", amount=200, price=2000)
boss.bossincome(name="Ламинат", amount=300, price=3000)
boss.sale(name="Ламинат", amount=100)
