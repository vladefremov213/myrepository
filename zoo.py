# Принято

class Animal:
    def __init__(self, name, space):
        self.name = name
        self.space = space

    def __str__(self):
        return self.name


class Cage:
    def __init__(self, space):
        self.space = space
        self.spisok = []

    def add_animal(self, animal):
        # По логике возвращаемое значение указывает, добавилось ли животное или нет
        # Если свободного места будет 200 и животное 200, то вернётся False
        if self.space >= animal.space:
            self.space -= animal.space
            self.spisok.append(animal.name)
            return self.space >= 0
        else:
            return self.space < 0    

    def get_animals(self):
        return self.spisok
        
    def free_space(self):
        return self.space


cage1 = Cage(300)
cage2 = Cage(200)

lion = Animal("Alex", 150)
pinguin1 = Animal("Skipper", 20)
pinguin2 = Animal("Kowalski", 15)
pinguin3 = Animal("Rico", 25)
begemoth = Animal("Gloria", 200)
giraffe = Animal("Melvin", 110)
zebra = Animal("Martin", 70)
print(cage1.add_animal(lion))      # True
print(cage2.add_animal(pinguin1))  # True
print(cage1.add_animal(pinguin2))  # True
print(cage2.add_animal(pinguin3))  # True
print(cage1.add_animal(begemoth))  # False
print(cage2.add_animal(giraffe))   # True
print(cage1.add_animal(zebra))     # True
print(cage1.free_space())          # 65
print(cage2.free_space())          # 45
print(cage1.get_animals())         # ['Alex', 'Kowalski', 'Martin']
print(cage2.get_animals())         # ['Skipper', 'Rico', 'Melvin']
