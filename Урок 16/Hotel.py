import sqlalchemy as sa
# Между импортами и кодом 2 пустые строки должно быть

metadata = sa.MetaData()
department_table = sa.Table("departments", metadata,
    sa.Column("id", sa.Integer, primary_key=True, autoincrement=True),
    sa.Column("name", sa.Unicode(64), unique=True, nullable=False),
)
position_table = sa.Table("position", metadata,
    sa.Column("id", sa.Integer, primary_key=True, autoincrement=True),
    sa.Column("name", sa.Unicode(64), unique=True, nullable=False),
    # 5 пробелов? Нехорошо
     sa.Column("department", sa.Integer, sa.ForeignKey("departments.id"), nullable=False),
)
staff_table = sa.Table("staff", metadata,
    sa.Column("id", sa.Integer, primary_key=True, autoincrement=True),
    sa.Column("name", sa.Unicode(64), nullable=False),
    sa.Column("surname", sa.Unicode(64), nullable=False),
    sa.Column("position", sa.Integer, sa.ForeignKey("position.id"), nullable=False),
)
room_class_table = sa.Table("room_class", metadata,
    sa.Column("id", sa.Integer, primary_key=True, autoincrement=True),
    sa.Column("class", sa.Unicode(64), unique=True, nullable=False),
    sa.Column("price", sa.Float, nullable=False),
)
room_table = sa.Table("rooms", metadata,
    sa.Column("id", sa.Integer, primary_key=True, autoincrement=True),
    sa.Column("number", sa.Integer, unique=True, nullable=False),
    sa.Column("class", sa.Integer, sa.ForeignKey("room_class.id"), nullable=False),
    sa.Column("is_free", sa.Boolean, nullable=False, default=True),
    sa.Column("is_booked", sa.Boolean, nullable=False, default=False),
)
client_table = sa.Table("clients", metadata,
    sa.Column("id", sa.Integer, primary_key=True, autoincrement=True),
    sa.Column("name", sa.Unicode(64), nullable=False),
    sa.Column("surname", sa.Unicode(64), nullable=False),
    sa.Column("checked_in_room", sa.Integer, sa.ForeignKey("rooms.id"), nullable=False),
)

engine = sa.create_engine("sqlite:///Hotel 'Green Elephant'.db")
metadata.create_all(engine)

departments = [
    {"name": "HR"},
    {"name": "Reception"},
    {"name": "Service"},
    {"name": "Finances"},
]
positions = [
    {"name": "Head of HR department", "department": "1"},
    {"name": "Senior HR", "department": "1"},
    {"name": "Junior HR", "department": "1"},
    {"name": "Head of Reception department", "department": "2"},
    {"name": "Senior receptionist", "department": "2"},
    {"name": "Junior receptionist", "department": "2"},
    {"name": "Head of Service department", "department": "3"},
    {"name": "Engineer", "department": "3"},
    {"name": "Cleaner", "department": "3"},
    {"name": "Head of Finance department", "department": "4"},
    {"name": "Senior accountant", "department": "4"},
    {"name": "Junior accountant","department": "4"}, 
]
staff = [
    {"name": "Ivan", "surname": "Ivanov", "position": "1"},
    {"name": "Petr", "surname": "Petrov", "position": "3"},
    {"name": "Sidor", "surname": "Sidorov", "position": "3"},
    {"name": "Alex", "surname": "Kuznetsov", "position": "4"},
    {"name": "Johny", "surname": "Depp", "position": "5"},
    {"name": "BoJack", "surname": "Horseman", "position": "7"},
    {"name": "Albert", "surname": "Einstein", "position": "9"},
    {"name": "Rick", "surname": "Sanches", "position": "9"},
    {"name": "Morty", "surname": "Smith", "position": "10"},
    {"name": "Gabe", "surname": "Newell", "position": "11"},   
]
room_class = [
    {"class": "cheap", "price": "10000"},
    {"class": "middle", "price": "15000"},
    {"class": "lux", "price": "20000"},
]
rooms = [
    {"number": "101", "class": "1", "is_free": False},
    {"number": "201", "class": "2", "is_free": False},
    {"number": "322", "class": "3", "is_free": True, "is_booked": True},
]
clients = [
    {"name": "Vladimir", "surname": "Epifancev", "checked_in_room": "1"},
    {"name": "Sergei", "surname": "Pahomov", "checked_in_room": "1"},
    {"name": "Maslaev", "surname": "Alexandr", "checked_in_room": "2"},
    {"name": "Anatoliy", "surname": "Osmolovskiy", "checked_in_room": "3"},
]
connection = engine.connect()
try:
    connection.execute(department_table.insert(), departments)
except sa.exc.IntegrityError:
    print("Already exists")
connection.execute(position_table.insert(), positions)
connection.execute(staff_table.insert(), staff)
connection.execute(room_class_table.insert(), room_class)
connection.execute(room_table.insert(), rooms)
connection.execute(client_table.insert(), clients)
