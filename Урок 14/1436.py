# Принято

n = int(input())
lst = [int(i) for i in input().split()]
for index in range(1, n):
    new_lst = lst.copy()
    while index > 0 and lst[index] < lst[index-1]:
        lst[index], lst[index-1] = lst[index-1], lst[index]
        index -= 1
    if new_lst != lst:
        for i in lst:
            print(i, end=" ")
        print()
