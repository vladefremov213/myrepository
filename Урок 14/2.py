# Принято

N, K = [int(i) for i in input().split()]
# В Python переменные не называют в стиле CamelCase
First = [int(i) for i in input().split()]
Second = [int(i) for i in input().split()]
for i in Second:
    left = 0
    right = N - 1
    while left < right:
        med = (left + right) // 2
        if First[med] < i:
            left = med + 1
        else:
            right = med
    if abs(First[left - 1] - i) <= abs(First[left] - i):
        print(First[left - 1])   
    else:
        print(First[left])
