# Принято

n = int(input())
lst = [int(i) for i in input().split()]
count = 0
for i in range(1, n):
    for j in range(0, n - i):
        if lst[j] > lst[j + 1]:
            lst[j], lst[j + 1] = lst[j + 1], lst[j]
            count += 1
print(count)
