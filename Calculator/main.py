from calc.operations import *
from calc.history import *
# Между импортами и кодом две пустые строки

print("""       Справка
Для начала работы нажмите Enter
Для сложения введите '+'
Для вычитания введите '-'
Для умножения введите '*'
Для деления введите '/'
Для возведения в степень введите '**'
Для вывода истории введите history
Для выхода введите exit""")
while True:
    s = input()
    if s == "history":
        reads()
        continue
    if s == "exit":
        break
    a = float(input("Введите первое число\n"))
    print("Выберите действие")
    c = input()
    b = float(input("Введите второе число\n"))
    # Очень много повторяющегося кода
    if c == "+":
        s1 = str(a) + " + " + str(b) + " = " + str(sum(a, b))
        print(s1)
        write(s1)
    elif c == "-":
        s1 = str(a) + " - " + str(b) + " = " + str(minus(a, b))
        print(s1)
        write(s1)
    elif c == "*":
        s1 = str(a) + " * " + str(b) + " = " + str(mult(a, b))
        print(s1)
        write(s1)
    elif c == "/":
        s1 = str(a) + " / " + str(b) + " = " + str(div(a, b))
        print(s1)
        write(s1)
    elif c == "**":
        s1 = str(a) + " ** " + str(b) + " = " + str(power(a, b))
        print(s1)
        write(s1)
