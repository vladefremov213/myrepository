# Не принято
# Нет скриншота

s = ""
letters = 0

with open("input.txt", "rt") as f:
    a = f.read(1)
    while a != "":
        s += a
        a = f.read(1)
        
for i in s:
    if i.isalpha():
        letters += 1      
words = len(s.split())
lines = len(s.split("\n"))

with open("output.txt", "wt") as f:
    f.write("Input file contains:\n") 
    f.write(str(letters) + " letters\n")
    f.write(str(words) + " words\n")    
    f.write(str(lines) + " lines")
