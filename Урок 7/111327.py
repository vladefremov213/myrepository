# Принято

with open("input.txt", "rt") as f:
    s = f.read()

a = 0
for i in s.split():
    a += int(i)

with open("output.txt", "wt") as f:    
    f.write(str(a))            
