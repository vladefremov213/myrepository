# Принято
def IsPointArea(x, y):
    return ((x + 1) ** 2 + (y - 1) ** 2 <= 4 and y >= 2 * x + 2 and y >= -x or 
           (x + 1) ** 2 + (y - 1) ** 2 >= 4 and y <= 2 * x + 2 and y <= -x and y >= -4)
       

x = float(input())
y = float(input())
# Какой тип данных возвращает IsPointArea? Какую роль играет сравнение с True?
if IsPointArea(x, y) == True:
    print("YES")
else:
    print("NO")
