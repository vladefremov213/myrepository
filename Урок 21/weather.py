import requests


session = requests.Session()
key = "32003c26cbcce7ed9894937cd51c724d"
uri = "https://api.openweathermap.org/data/2.5/weather"
cityname = input()
response = session.get(
    url=uri,
    params={
        "q": cityname,
        "appid": key,
        "units": "metric",
    }
)
d = response.json()
print(f'Температура в городе {cityname} - {round(d["main"]["temp"])}°C')
