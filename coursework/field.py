from random import randint

from threading import Thread

import socket

import pickle

import pygame

import fish

import menu


class Field:
    def __init__(self, screen, mode, p1_points=0, p2_points=0, timer=60, clock=1):
        self.mode = mode
        if mode == "Easy":
            pygame.time.set_timer(pygame.USEREVENT+1, 1500)
        elif mode == "Medium":
            pygame.time.set_timer(pygame.USEREVENT+1, 1000)
        elif mode == "Hard":
            pygame.time.set_timer(pygame.USEREVENT+1, 650)
        self.p1_points = p1_points
        self.p2_points = p2_points
        self.timer = timer
        self.clock = clock
        self.pos_x = (randint(0, 960))
        self.pos_y = (randint(64, 702))
        self.screen = screen
        self.name = ""
        self.fish_type = randint(1, 100)
        if self.fish_type > 40:
            self.element = fish.Fish(shrimp_image)
            self.name = "Shrimp"
        elif self.fish_type > 5:
            self.element = fish.Fish(clown_image)
            self.name = "Clown"
        else:
            self.element = fish.Fish(horse_image)
            self.name = "Horse"


    def draw_field(self):
        pygame.display.update()
        self.screen.blit(b_g, (0, 0))
        self.element.draw(self.screen, (self.pos_x, self.pos_y))
        text = font.render(f"Счет: {self.p1_points}", 0, (0, 0, 0))
        text_rect = text.get_rect(topleft=(20, 20))
        self.screen.blit(text, text_rect)


    def draw_timer(self, start_timer):
        self.clock = round(self.timer - (pygame.time.get_ticks()  - start_timer) / 1000)
        timer_text = font.render(f"Оставшееся время: {self.clock}", 0, (0, 0, 0))
        timer_rect = timer_text.get_rect(topright=(1004, 20))
        self.screen.blit(timer_text, timer_rect)


    def myget_rect(self):
        return pygame.Rect(self.pos_x, self.pos_y, 64, 64)


    def start_field(self, start_timer):
        while self.clock > 0:
            self.draw_field()
            self.draw_timer(start_timer)
            events = pygame.event.get()
            for event in events:
                if event.type == pygame.QUIT:
                    pygame.quit()
                if event.type == pygame.USEREVENT+1:
                    self.__init__(self.screen, self.mode, self.p1_points, self.timer)
                if event.type == pygame.MOUSEBUTTONDOWN:
                    if self.myget_rect().collidepoint(event.pos):
                        if self.name == "Shrimp":
                            self.p1_points += 1
                        elif self.name == "Clown":
                            self.p1_points += 10
                        else:
                            self.p1_points += 100
                        self.__init__(self.screen, self.mode, self.p1_points, self.timer)
                    else:
                        self.p1_points -= 10
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_ESCAPE:
                        pausemenu = menu.Menu(self.screen)
                        pausemenu.pause_start(self.mode, self.p1_points, self.clock)

        newmenu = menu.Menu(self.screen)
        newmenu.result_compare(self.p1_points, self.mode)


class ServerField(Field):
    def connect(self):
        self.sock = socket.socket()
        try:
            self.sock.bind(("", 8000))
        except OSError:
            newmenu = menu.Menu(self.screen)
            newmenu.mp_start()
        self.sock.listen(1)
        self.conn, self.addr = self.sock.accept()
        self.send_params()
        start_timer = pygame.time.get_ticks()
        self.start_field(start_timer)


    def close(self):
        self.sock.close()


    def draw_field(self):
        pygame.display.update()
        self.screen.blit(b_g, (0, 0))
        self.element.draw(self.screen, (self.pos_x, self.pos_y))
        p1_text = font.render(f"Игрок 1: {self.p1_points}", 0, (0, 0, 0))
        p2_text = font.render(f"Игрок 2: {self.p2_points}", 0, (0, 0, 0))
        p1_text_rect = p1_text.get_rect(topleft=(20, 10))
        p2_text_rect = p2_text.get_rect(topleft=(20, 40))
        self.screen.blit(p1_text, p1_text_rect)
        self.screen.blit(p2_text, p2_text_rect)


    def request(self):
        try:
            while self.thread:
                request = self.conn.recv(1024).decode("utf-8")
                if request == "Shrimp":
                    self.p2_points += 1
                    self.__init__(self.screen, self.mode, self.p1_points,
                                  self.p2_points, self.timer)
                    self.send_params()
                elif request == "Clown":
                    self.p2_points += 10
                    self.__init__(self.screen, self.mode, self.p1_points,
                                  self.p2_points, self.timer)
                    self.send_params()
                elif request == "Horse":
                    self.p2_points += 100
                    self.__init__(self.screen, self.mode, self.p1_points,
                                  self.p2_points, self.timer)
                    self.send_params()
                else:
                    self.p2_points -= 10
                    self.send_params()
        except ConnectionResetError:
            self.close()


    def send_params(self):
        params = {}
        params["fishtype"] = self.fish_type
        params["pos_x"] = self.pos_x
        params["pos_y"] = self.pos_y
        params["p1points"] = self.p1_points
        params["p2points"] = self.p2_points
        obj = pickle.dumps(params, 2)
        self.conn.send(obj)


    def start_field(self, start_timer):
        self.thread = True
        thread2 = Thread(target=self.request)
        thread2.start()
        while self.clock > 0:
            self.draw_field()
            self.draw_timer(start_timer)
            events = pygame.event.get()
            for event in events:
                if event.type == pygame.QUIT:
                    pygame.quit()
                    self.close()
                if event.type == pygame.USEREVENT+1:
                    self.__init__(self.screen, self.mode, self.p1_points,
                                  self.p2_points, self.timer)
                    self.send_params()
                if event.type == pygame.MOUSEBUTTONDOWN:
                    if self.myget_rect().collidepoint(event.pos):
                        if self.name == "Shrimp":
                            self.p1_points += 1
                        elif self.name == "Clown":
                            self.p1_points += 10
                        else:
                            self.p1_points += 100
                        self.__init__(self.screen, self.mode, self.p1_points,
                                      self.p2_points, self.timer)
                        self.send_params()
                    else:
                        self.p1_points -= 10
                        self.send_params()
        self.thread = False
        self.close()
        newmenu = menu.Menu(self.screen)
        newmenu.mp_result_start(self.p1_points, self.p2_points)


class ClientField(Field):
    def __init__(self, screen, mode, fishtype=0, pos_x=0, pos_y=0,
                 p1_points=0, p2_points=0, timer=60):
        self.mode = mode
        self.p1_points = p1_points
        self.p2_points = p2_points
        self.timer = timer
        self.clock = 1
        self.pos_x = pos_x
        self.pos_y = pos_y
        self.screen = screen
        self.name = ""
        self.fish_type = fishtype
        if self.fish_type > 40:
            self.element = fish.Fish(shrimp_image)
            self.name = "Shrimp"
        elif self.fish_type > 5:
            self.element = fish.Fish(clown_image)
            self.name = "Clown"
        else:
            self.element = fish.Fish(horse_image)
            self.name = "Horse"


    def connect(self):
        self.sock = socket.socket()
        self.addr = ("127.0.0.1", 8000)
        try:
            self.sock.connect(self.addr)
        except ConnectionRefusedError:
            newmenu = menu.Menu(self.screen)
            newmenu.mp_start()
        start_timer = pygame.time.get_ticks()
        self.start_field(start_timer)


    def close(self):
        self.sock.close()


    def draw_field(self):
        pygame.display.update()
        self.screen.blit(b_g, (0, 0))
        self.element.draw(self.screen, (self.pos_x, self.pos_y))
        p1_text = font.render(f"Игрок 1: {self.p1_points}", 0, (0, 0, 0))
        p2_text = font.render(f"Игрок 2: {self.p2_points}", 0, (0, 0, 0))
        p1_text_rect = p1_text.get_rect(topleft=(20, 10))
        p2_text_rect = p2_text.get_rect(topleft=(20, 40))
        self.screen.blit(p1_text, p1_text_rect)
        self.screen.blit(p2_text, p2_text_rect)


    def get_params(self):
        obj = self.sock.recv(1024)
        self.params = pickle.loads(obj)


    def request(self):
        try:
            while self.thread:
                self.get_params()
                self.__init__(self.screen, self.mode, self.params["fishtype"], self.params["pos_x"],
                              self.params["pos_y"], self.params["p1points"],
                              self.params["p2points"], self.timer)
        except ConnectionResetError:
            pass


    def start_field(self, start_timer):
        self.thread = True
        thread1 = Thread(target=self.request)
        thread1.start()
        while self.clock > 0:
            self.draw_field()
            self.draw_timer(start_timer)
            events = pygame.event.get()
            for event in events:
                if event.type == pygame.QUIT:
                    pygame.quit()
                    self.close()
                if event.type == pygame.MOUSEBUTTONDOWN:
                    if self.myget_rect().collidepoint(event.pos):
                        if self.name == "Shrimp":
                            self.sock.send("Shrimp".encode("utf-8"))
                        elif self.name == "Clown":
                            self.sock.send("Clown".encode("utf-8"))
                        else:
                            self.sock.send("Horse".encode("utf-8"))
                        self.__init__(self.screen, self.mode, self.params["fishtype"],
                                      self.params["pos_x"], self.params["pos_y"],
                                      self.params["p1points"], self.params["p2points"], self.timer)
                    else:
                        self.p2_points -= 10
                        self.sock.send("Miss".encode("utf-8"))
        self.thread = False
        self.close()
        newmenu = menu.Menu(self.screen)
        newmenu.mp_result_start(self.p1_points, self.p2_points)

shrimp_image = pygame.image.load("images/shrimp.png")
clown_image = pygame.image.load("images/clown.png")
horse_image = pygame.image.load("images/seahorse.png")
b_g = pygame.image.load("images/sea.jpg")
pygame.font.init()
font = pygame.font.Font("font/font.ttf", 28)
