import pygame

import menu

class Game:
    def __init__(self, resolution):
        self.resolution = resolution
        self.clock = pygame.time.Clock()
        self.screen = pygame.display.set_mode(resolution)
        self.menu = menu.Menu(self.screen)


    def run(self):
        self.menu.start_menu()
