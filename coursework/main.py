import pygame

import game


pygame.init()
pygame.display.set_caption("Fish CLicker")
mygame = game.Game(resolution=(1024, 768))

if __name__ == "__main__":
    mygame.run()
