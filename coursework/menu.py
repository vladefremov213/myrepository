from threading import Thread

import pygame

import pygame_textinput

import field


class Menu:
    def __init__(self, screen):
        self.menu = True
        self.result = True
        self.font = pygame.font.Font("font/font.ttf", 40)
        self.faq_font = pygame.font.Font("font/font.ttf", 22)
        self.screen = screen
        self.screen.fill((0, 10, 160))


    def elements(self, text, alias, color, background, center):
        element = self.font.render(text, alias, color)
        rect = element.get_rect(center=center)
        width = rect.width + 20
        height = rect.height
        surf = pygame.Surface((width, height))
        surf.fill((background))
        self.screen.blit(surf, (rect[0] - 10, rect[1] + 2))
        self.screen.blit(element, rect)
        return rect


    def draw_menu(self):
        pygame.display.update()
        self.screen.fill((0, 10, 160))
        self.menu_start = self.elements("Старт", 0, (255, 255, 153), (0, 10, 160),
                                        (512, 182))
        self.menu_mp = self.elements("Мультиплеер", 0, (255, 255, 153), (0, 10, 160),
                                     (512, 282))
        self.menu_faq = self.elements("Справка", 0, (255, 255, 153), (0, 10, 160),
                                      (512, 382))
        self.menu_leaders = self.elements("Лидеры", 0, (255, 255, 153), (0, 10, 160),
                                          (512, 482))
        self.menu_exit = self.elements("Выход", 0, (255, 255, 153), (0, 10, 160),
                                       (512, 582))
        pos_x, pos_y = pygame.mouse.get_pos()
        if self.menu_start.collidepoint(pos_x, pos_y):
            self.menu_start = self.elements("Старт", 0, (255, 255, 153), (200, 0, 0),
                                            (512, 182))
        if self.menu_faq.collidepoint(pos_x, pos_y):
            self.menu_faq = self.elements("Справка", 0, (255, 255, 153), (200, 0, 0),
                                          (512, 382))
        if self.menu_leaders.collidepoint(pos_x, pos_y):
            self.menu_leaders = self.elements("Лидеры", 0, (255, 255, 153), (200, 0, 0),
                                              (512, 482))
        if self.menu_exit.collidepoint(pos_x, pos_y):
            self.menu_exit = self.elements("Выход", 0, (255, 255, 153), (200, 0, 0),
                                           (512, 582))
        if self.menu_mp.collidepoint(pos_x, pos_y):
            self.menu_mp = self.elements("Мультиплеер", 0, (255, 255, 153), (200, 0, 0),
                                         (512, 282))


    def mode_draw(self):
        pygame.display.update()
        self.screen.fill((0, 10, 160))
        self.mode_easy = self.elements("Легко", 0, (255, 255, 153), (0, 10, 160),
                                       (512, 232))
        self.mode_medium = self.elements("Средне", 0, (255, 255, 153), (0, 10, 160),
                                         (512, 332))
        self.mode_hard = self.elements("Сложно", 0, (255, 255, 153), (0, 10, 160),
                                       (512, 432))
        self.back = self.elements("Назад", 0, (255, 255, 153), (0, 10, 160), (512, 532))
        pos_x, pos_y = pygame.mouse.get_pos()
        if self.mode_easy.collidepoint(pos_x, pos_y):
            self.mode_easy = self.elements("Легко", 0, (255, 255, 153), (200, 0, 0),
                                           (512, 232))
        if self.mode_medium.collidepoint(pos_x, pos_y):
            self.mode_medium = self.elements("Средне", 0, (255, 255, 153), (200, 0, 0),
                                             (512, 332))
        if self.mode_hard.collidepoint(pos_x, pos_y):
            self.mode_hard = self.elements("Сложно", 0, (255, 255, 153), (200, 0, 0),
                                           (512, 432))
        if self.back.collidepoint(pos_x, pos_y):
            self.back = self.elements("Назад", 0, (255, 255, 153), (200, 0, 0), (512, 532))


    def mode_start(self):
        while self.menu:
            self.mode_draw()
            events = pygame.event.get()
            for event in events:
                if event.type == pygame.QUIT:
                    pygame.quit()
                if event.type == pygame.MOUSEBUTTONDOWN:
                    if self.mode_easy.collidepoint(event.pos):
                        self.mode = "Easy"
                        self.menu = False
                        newfield = field.Field(self.screen, self.mode)
                        start_timer = pygame.time.get_ticks()
                        newfield.start_field(start_timer)
                    if self.mode_medium.collidepoint(event.pos):
                        self.mode = "Medium"
                        self.menu = False
                        newfield = field.Field(self.screen, self.mode)
                        start_timer = pygame.time.get_ticks()
                        newfield.start_field(start_timer)
                    if self.mode_hard.collidepoint(event.pos):
                        self.mode = "Hard"
                        self.menu = False
                        newfield = field.Field(self.screen, self.mode)
                        start_timer = pygame.time.get_ticks()
                        newfield.start_field(start_timer)
                    if self.back.collidepoint(event.pos):
                        self.start_menu()
                        self.menu = False


    def faq_draw(self):
        pygame.display.update()
        self.screen.fill((0, 10, 160))
        self.back = self.elements("Назад", 0, (255, 255, 153), (0, 10, 160), (512, 582))
        faq_x = 20
        faq_y = 20
        for i in range(0, len(faq)):
            faq_text = self.faq_font.render(faq[i].strip(), 0, (255, 255, 153))
            faq_rect = faq_text.get_rect(topleft=(faq_x, faq_y))
            self.screen.blit(faq_text, faq_rect)
            faq_y += 30
        pos_x, pos_y = pygame.mouse.get_pos()
        if self.back.collidepoint(pos_x, pos_y):
            self.back = self.elements("Назад", 0, (255, 255, 153), (200, 0, 0), (512, 582))


    def faq_start(self):
        while self.menu:
            self.faq_draw()
            events = pygame.event.get()
            for event in events:
                if event.type == pygame.QUIT:
                    pygame.quit()
                if event.type == pygame.MOUSEBUTTONDOWN:
                    if self.back.collidepoint(event.pos):
                        self.start_menu()
                        self.menu = False


    def leaders_draw(self):
        pygame.display.update()
        self.screen.fill((0, 10, 160))
        self.back = self.elements("Назад", 0, (255, 255, 153), (0, 10, 160), (512, 582))
        self.medium = self.elements("Средне", 0, (255, 255, 153), (0, 10, 160), (512, 30))
        self.hard = self.elements("Сложно", 0, (255, 255, 153), (0, 10, 160), (862, 30))
        self.easy = self.elements("Легко", 0, (255, 255, 153), (0, 10, 160), (162, 30))
        leaders_x = 162
        leaders_y = 80
        counter_easy = 1

        for i in easy_leaders:
            easy_text = self.faq_font.render(f"{counter_easy}. {i[0]} {i[1]}", 0, (255, 255, 153))
            easy_rect = easy_text.get_rect(center=(leaders_x, leaders_y))
            self.screen.blit(easy_text, easy_rect)
            leaders_y += 30
            counter_easy += 1

        leaders_y = 80
        counter_medium = 1

        for i in medium_leaders:
            medium_text = self.faq_font.render(f"{counter_medium}. {i[0]} {i[1]}",
                                               0, (255, 255, 153))
            medium_rect = medium_text.get_rect(center=(leaders_x + 350, leaders_y))
            self.screen.blit(medium_text, medium_rect)
            leaders_y += 30
            counter_medium += 1

        leaders_y = 80
        counter_hard = 1

        for i in hard_leaders:
            hard_text = self.faq_font.render(f"{counter_hard}. {i[0]} {i[1]}", 0, (255, 255, 153))
            hard_rect = hard_text.get_rect(center=(leaders_x + 700, leaders_y))
            self.screen.blit(hard_text, hard_rect)
            leaders_y += 30
            counter_hard += 1

        pos_x, pos_y = pygame.mouse.get_pos()
        if self.back.collidepoint(pos_x, pos_y):
            self.back = self.elements("Назад", 0, (255, 255, 153), (200, 0, 0), (512, 582))


    def leaders_get(self):
        with open("leaders/easy.txt", "rt", encoding="utf-8") as easy_file:
            lines = easy_file.readlines()
            for line in lines:
                easy_leaders.append(line.strip().split(" "))
            easy_file.close()

        with open("leaders/medium.txt", "rt", encoding="utf-8") as medium_file:
            lines = medium_file.readlines()
            for line in lines:
                medium_leaders.append(line.strip().split(" "))
            medium_file.close()

        with open("leaders/hard.txt", "rt", encoding="utf-8") as hard_file:
            lines = hard_file.readlines()
            for line in lines:
                hard_leaders.append(line.strip().split(" "))
            hard_file.close()


    def leaders_start(self):
        self.leaders_get()
        while self.menu:
            self.leaders_draw()
            events = pygame.event.get()
            for event in events:
                if event.type == pygame.QUIT:
                    pygame.quit()
                if event.type == pygame.MOUSEBUTTONDOWN:
                    if self.back.collidepoint(event.pos):
                        easy_leaders.clear()
                        medium_leaders.clear()
                        hard_leaders.clear()
                        self.start_menu()
                        self.menu = False


    def result_draw(self, result):
        pygame.display.update()
        self.screen.fill((0, 10, 160))
        self.elements("Время вышло!", 0, (255, 255, 153), (0, 10, 160), (512, 282))
        self.elements(f"Ваш результат: {result}", 0, (255, 255, 153), (0, 10, 160), (512, 382))
        self.result_back = self.elements("В меню", 0, (255, 255, 153), (0, 10, 160), (382, 482))
        self.result_retry = self.elements("Повтор ", 0, (255, 255, 153), (0, 10, 160), (632, 482))
        pos_x, pos_y = pygame.mouse.get_pos()
        if self.result_back.collidepoint(pos_x, pos_y):
            self.result_back = self.elements("В меню", 0, (255, 255, 153), (200, 0, 0),
                                             (382, 482))
        if self.result_retry.collidepoint(pos_x, pos_y):
            self.result_retry = self.elements("Повтор", 0, (255, 255, 153), (200, 0, 0),
                                              (632, 482))


    def text_input(self, result):
        self.textinput = pygame_textinput.TextInput(font_size=60, antialias=False,
                                                    text_color=(255, 255, 153),
                                                    cursor_color=(255, 255, 153))
        inputing = True
        while inputing:
            pygame.display.update()
            self.screen.fill((0, 10, 160))
            self.elements(f"Новый рекорд!: {result}", 0, (255, 255, 153), (0, 10, 160), (512, 282))
            self.elements("Введите имя", 0, (255, 255, 153), (0, 10, 160), (512, 382))
            self.screen.blit(self.textinput.get_surface(), (400, 482))

            events = pygame.event.get()
            self.textinput.update(events)
            for event in events:
                if event.type == pygame.QUIT:
                    pygame.quit()
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_RETURN:
                        name = self.textinput.get_text().strip()
                        inputing = False
        return name


    def result_compare(self, result, mode):
        self.leaders_get()
        if mode == "Easy":
            counter = 0
            for i in easy_leaders:
                if result > 0 and (result > int(i[1]) or len(easy_leaders) < 10):
                    name = self.text_input(result)
                    easy_leaders.append([name, result])
                    easy_leaders.sort(key=lambda a: int(a[1]), reverse=True)
                    break

            easy_file1 = open("leaders/easy.txt", "w", encoding="utf-8")
            for i in easy_leaders:
                easy_file1.write(f"{i[0]} {i[1]}\n")
                counter += 1
                if counter == 10:
                    break
            easy_file1.close()

            self.result_start(result, mode)

        if mode == "Medium":
            counter = 0
            for i in medium_leaders:
                if result > 0 and (result > int(i[1]) or len(medium_leaders) < 10):
                    name = self.text_input(result)
                    medium_leaders.append([name, result])
                    medium_leaders.sort(key=lambda a: int(a[1]), reverse=True)
                    break

            medium_file1 = open("leaders/medium.txt", "w", encoding="utf-8")
            for i in medium_leaders:
                medium_file1.write(f"{i[0]} {i[1]}\n")
                counter += 1
                if counter == 10:
                    break
            medium_file1.close()

            self.result_start(result, mode)

        if mode == "Hard":
            counter = 0
            for i in hard_leaders:
                if result > 0 and (result > int(i[1]) or len(hard_leaders) < 10):
                    name = self.text_input(result)
                    hard_leaders.append([name, result])
                    hard_leaders.sort(key=lambda a: int(a[1]), reverse=True)
                    break

            hard_file1 = open("leaders/hard.txt", "w", encoding="utf-8")
            for i in hard_leaders:
                hard_file1.write(f"{i[0]} {i[1]}\n")
                counter += 1
                if counter == 10:
                    break
            hard_file1.close()

            self.result_start(result, mode)


    def result_start(self, result, mode):
        easy_leaders.clear()
        medium_leaders.clear()
        hard_leaders.clear()
        while self.result:
            self.result_draw(result)
            events = pygame.event.get()
            for event in events:
                if event.type == pygame.QUIT:
                    pygame.quit()
                if event.type == pygame.MOUSEBUTTONDOWN:
                    if self.result_back.collidepoint(event.pos):
                        self.result = False
                        self.start_menu()
                    if self.result_retry.collidepoint(event.pos):
                        self.result = False
                        newfield = field.Field(self.screen, mode)
                        start_timer = pygame.time.get_ticks()
                        newfield.start_field(start_timer)


    def mp_draw(self):
        pygame.display.update()
        self.screen.fill((0, 10, 160))
        self.mp_server = self.elements("Сервер", 0, (255, 255, 153), (0, 10, 160),
                                       (512, 282))
        self.mp_client = self.elements("Клиент", 0, (255, 255, 153), (0, 10, 160),
                                       (512, 382))
        self.mp_back = self.elements("Назад", 0, (255, 255, 153), (0, 10, 160),
                                     (512, 482))
        pos_x, pos_y = pygame.mouse.get_pos()
        if self.mp_server.collidepoint(pos_x, pos_y):
            self.mp_server = self.elements("Сервер", 0, (255, 255, 153), (200, 0, 0),
                                           (512, 282))
        if self.mp_client.collidepoint(pos_x, pos_y):
            self.mp_client = self.elements("Клиент", 0, (255, 255, 153), (200, 0, 0),
                                           (512, 382))
        if self.mp_back.collidepoint(pos_x, pos_y):
            self.mp_back = self.elements("Назад", 0, (255, 255, 153), (200, 0, 0),
                                         (512, 482))


    def mp_start(self):
        while self.menu:
            self.mp_draw()
            events = pygame.event.get()
            for event in events:
                if event.type == pygame.QUIT:
                    pygame.quit()
                if event.type == pygame.MOUSEBUTTONDOWN:
                    if self.mp_server.collidepoint(event.pos):
                        self.menu = False
                        serverfield = field.ServerField(self.screen, "Medium")
                        thread1 = Thread(target=serverfield.connect())
                        thread1.start()
                    if self.mp_client.collidepoint(event.pos):
                        self.menu = False
                        self.clientfield = field.ClientField(self.screen, "Medium")
                        thread2 = Thread(target=self.clientfield.connect())
                        thread2.start()
                    if self.mp_back.collidepoint(event.pos):
                        self.start_menu()
                        self.menu = False


    def mp_result_draw(self, result1, result2):
        pygame.display.update()
        self.screen.fill((0, 10, 160))
        self.elements("Время вышло!", 0, (255, 255, 153), (0, 10, 160), (512, 282))
        if result1 > result2:
            self.elements("Победил Игрок 1!", 0, (255, 255, 153), (0, 10, 160), (512, 382))
        elif result1 < result2:
            self.elements("Победил Игрок 2!", 0, (255, 255, 153), (0, 10, 160), (512, 382))
        else:
            self.elements("Ничья!", 0, (255, 255, 153), (0, 10, 160), (512, 382))
        self.mp_result_back = self.elements("В меню", 0, (255, 255, 153), (0, 10, 160), (512, 482))
        pos_x, pos_y = pygame.mouse.get_pos()
        if self.mp_result_back.collidepoint(pos_x, pos_y):
            self.mp_result_back = self.elements("В меню", 0, (255, 255, 153), (200, 0, 0),
                                                (512, 482))


    def mp_result_start(self, result1, result2):
        while self.result:
            self.mp_result_draw(result1, result2)
            events = pygame.event.get()
            for event in events:
                if event.type == pygame.QUIT:
                    pygame.quit()
                if event.type == pygame.MOUSEBUTTONDOWN:
                    if self.mp_result_back.collidepoint(event.pos):
                        self.result = False
                        self.start_menu()


    def pause_draw(self):
        pygame.display.update()
        self.screen.fill((0, 10, 160))
        self.pause_continue = self.elements("Продолжить", 0, (255, 255, 153), (0, 10, 160),
                                            (512, 282))
        self.pause_retry = self.elements("Заново", 0, (255, 255, 153), (0, 10, 160),
                                         (512, 382))
        self.pause_menu = self.elements("Меню", 0, (255, 255, 153), (0, 10, 160),
                                        (512, 482))
        pos_x, pos_y = pygame.mouse.get_pos()
        if self.pause_continue.collidepoint(pos_x, pos_y):
            self.pause_continue = self.elements("Продолжить", 0, (255, 255, 153), (200, 0, 0),
                                                (512, 282))
        if self.pause_retry.collidepoint(pos_x, pos_y):
            self.pause_retry = self.elements("Заново", 0, (255, 255, 153), (200, 0, 0),
                                             (512, 382))
        if self.pause_menu.collidepoint(pos_x, pos_y):
            self.pause_menu = self.elements("Меню", 0, (255, 255, 153), (200, 0, 0),
                                            (512, 482))


    def pause_start(self, mode, points, timer):
        while self.menu:
            self.pause_draw()
            events = pygame.event.get()
            for event in events:
                if event.type == pygame.QUIT:
                    pygame.quit()
                if event.type == pygame.MOUSEBUTTONDOWN:
                    if self.pause_continue.collidepoint(event.pos):
                        self.menu = False
                        newfield = field.Field(self.screen, mode, points, points, timer)
                        start_timer = pygame.time.get_ticks()
                        newfield.start_field(start_timer)
                    if self.pause_retry.collidepoint(event.pos):
                        self.menu = False
                        newfield = field.Field(self.screen, mode)
                        start_timer = pygame.time.get_ticks()
                        newfield.start_field(start_timer)
                    if self.pause_menu.collidepoint(event.pos):
                        self.start_menu()
                        self.menu = False


    def start_menu(self):
        while self.menu:
            self.draw_menu()
            events = pygame.event.get()
            for event in events:
                if event.type == pygame.QUIT:
                    pygame.quit()
                if event.type == pygame.MOUSEBUTTONDOWN:
                    if self.menu_start.collidepoint(event.pos):
                        self.mode_start()
                    if self.menu_leaders.collidepoint(event.pos):
                        self.leaders_start()
                    if self.menu_exit.collidepoint(event.pos):
                        pygame.quit()
                    if self.menu_faq.collidepoint(event.pos):
                        self.faq_start()
                    if self.menu_mp.collidepoint(event.pos):
                        self.mp_start()


faq = []
easy_leaders = []
medium_leaders = []
hard_leaders = []

with open("faq.txt", "r", encoding="utf-8") as faq_file:
    flines = faq_file.readlines()
    for fline in flines:
        faq.append(fline)
    faq_file.close()
 