class Fish:
    def __init__(self, image):
        self.image = image

    def draw(self, screen, *coords):
        screen.blit(self.image, *coords)
